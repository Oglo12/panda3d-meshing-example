"""

TODO:

    If possible, make the panda.gif file actually animate somehow.

"""

from panda3d.core import loadPrcFile
loadPrcFile("config.prc")

from panda3d.core import GeomVertexFormat, GeomVertexData, GeomVertexWriter
from panda3d.core import Geom, GeomNode, GeomTriangles
from panda3d.core import TransparencyAttrib
from panda3d.core import OrthographicLens
from panda3d.core import SamplerState, Texture

from direct.showbase.ShowBase import ShowBase

def create_rectangle(x, z, width, height, color=None, shift=False, shiftBy=50):
    _format = GeomVertexFormat.getV3c4t2()
    vdata = GeomVertexData('square', _format, Geom.UHDynamic)

    xtop = x

    if shift == True:
        xtop += shiftBy

    vertex = GeomVertexWriter(vdata, "vertex")
    color_gvw = GeomVertexWriter(vdata, "color")
    texcoord = GeomVertexWriter(vdata, "texcoord")
    tris = GeomTriangles(Geom.UHDynamic)

    vertex.addData3(xtop, 0, z)
    vertex.addData3(x, 0, z - height)
    vertex.addData3(x + width, 0, z - height)
    vertex.addData3(xtop + width, 0, z)

    if color != None:
        if len(color) < 4:
            color = (1.0, 1.0, 1.0, 1.0)
        color_gvw.addData4f(color)
        color_gvw.addData4f(color)
        color_gvw.addData4f(color)
        color_gvw.addData4f(color)
    else:
        color_gvw.addData4f(1.0, 0.0, 0.0, 1.0)
        color_gvw.addData4f(1.0, 0.3, 0.0, 1.0)
        color_gvw.addData4f(1.0, 0.5, 0.0, 1.0)
        color_gvw.addData4f(1.0, 0.8, 0.0, 1.0)
    
    texcoord.addData2f(0.0, 1.0)
    texcoord.addData2f(0.0, 0.0)
    texcoord.addData2f(1.0, 0.0)
    texcoord.addData2f(1.0, 1.0)

    tris.addVertices(0, 1, 2)
    tris.addVertices(2, 3, 0)

    square = Geom(vdata)
    square.addPrimitive(tris)

    return square

class App(ShowBase):
    def __init__(self):
        super().__init__()

        self.setBackgroundColor(0.04, 0.04, 0.04)

        self.cam.setPos(0, -1111, -50)

        square1 = create_rectangle(-100, 234, 200, 200)
        square2 = create_rectangle(120, 0, 300, 200, (1, 0.3, 1, 1))
        square3 = create_rectangle(-420, -50, 200, 200, (1, 1, 0.3, 1), True, 123)

        gnode = GeomNode('square')

        gnode.addGeom(square1)
        gnode.addGeom(square2)
        gnode.addGeom(square3)

        square1gif = create_rectangle(-100, -134, 200, 200, (1, 1, 1))

        gifnode = GeomNode('square')

        gifnode.addGeom(square1gif)

        texture = self.loader.loadTexture("white_camo.png")
        texture.setMagfilter(SamplerState.FT_nearest) # To stop the texture from being blurry.
        texture.setMinfilter(SamplerState.FT_nearest) # ^

        gif = self.loader.loadTexture("panda.gif")

        rectanglesNode = self.render.attachNewNode(gnode) # Finally! Something I can use normally!
        rectanglesNode.setTransparency(TransparencyAttrib.MAlpha) # So transparent textures show up correctly.
        rectanglesNode.setTexture(texture)

        gifNode = self.render.attachNewNode(gifnode) # Finally! Something I can use normally!
        gifNode.setTransparency(TransparencyAttrib.MAlpha) # So transparent textures show up correctly.
        gifNode.setTexture(gif)

app = App()
app.run()